# php-extended/php-integer-capacity-object
A library that implements the php-extended/php-integer-capacity-interface package

![coverage](https://gitlab.com/php-extended/php-integer-capacity-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-integer-capacity-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-integer-capacity-object ^8`


## Basic Usage

This library provides 4 classes to specify integer capacities from different
containers.

- The `BitIntegerCapacity` class uses bits to count the capacity of integers
- The `ByteIntegerCapacity` class uses 8-bits bytes to count the capacity of integers
- The `StringIntegerCapacity` class uses decimal digits arranged in a string to count the capacity of integers
- The `HexaIntegerCapacity` class uses hexadecimal digits arranged in a string to count the capacity of integers.

All those classes are usable the following way:

```php

use PhpExtended\Model\BitIntegerCapacity;
use PhpExtended\Model\ByteIntegerCapacity;
use PhpExtended\Model\StringIntegerCapacity;

$capacity = new BitIntegerCapacity(32);
// $capacity is now a 32-bit integer capacity

$capacity = new ByteIntegerCapacity($capacity);
// $capacity is now a 4-byte integer capacity

$capacity = new StringIntegerCapacity($capacity);
// $capacity is now a 10-digits integer capacity

$capacity = new BitIntegerCapacity($capacity);
// $capacity is now a 33-bit integer capacity
// note that due to roundings and the ever expanding 
// available maximum value, the number of bits needed to
// encode the same information increases !

```

All those classes may be built with a positive non-zero integer as capacity,
or another instance of the `PhpExtended\Model\IntegerCapacityInterface`.

A resolver is also provided to unserialize the data given by the `__toString()`
method that may be used with integer capacities.

```

use PhpExtended\Model\IntegerCapacityResolver;

$resolver = new IntegerCapacityResolver();
$capacity = $resolver->resolveCapacity('8 bits');
// $capacity is now a BitIntegerCapacity with 8 as bit value.

```


## License

MIT (See [license file](LICENSE)).
