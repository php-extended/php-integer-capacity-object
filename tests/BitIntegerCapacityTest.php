<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Model\BitIntegerCapacity;
use PHPUnit\Framework\TestCase;

/**
 * BitIntegerCapacityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\BitIntegerCapacity
 *
 * @internal
 *
 * @small
 */
class BitIntegerCapacityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BitIntegerCapacity
	 */
	protected BitIntegerCapacity $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('12 bits', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BitIntegerCapacity(12);
	}
	
}
