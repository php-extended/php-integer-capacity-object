<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Model\StringIntegerCapacity;
use PHPUnit\Framework\TestCase;

/**
 * StringIntegerCapacityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\StringIntegerCapacity
 *
 * @internal
 *
 * @small
 */
class StringIntegerCapacityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StringIntegerCapacity
	 */
	protected StringIntegerCapacity $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('12 chars', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StringIntegerCapacity(12);
	}
	
}
