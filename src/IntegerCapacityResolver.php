<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * IntegerCapacityResolver class file.
 * 
 * This class provides a method to transform the string that represents an
 * integer capacity into a capacity object.
 * 
 * @author Anastaszor
 */
class IntegerCapacityResolver implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the capacity from the value string. This throws an exception in
	 * case the parsing is not possible.
	 * 
	 * @param string $value
	 * @return IntegerCapacityInterface
	 */
	public function resolveCapacity(string $value) : IntegerCapacityInterface
	{
		$matches = [];
		if(!\preg_match('#^(\\d+) (\\w+)$#', \trim($value), $matches))
		{
			return new ByteIntegerCapacity(1);
		}
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$capacity = (int) $matches[0];
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$unit = $matches[1];
		
		switch(\trim((string) \mb_strtolower($unit), 's'))
		{
			case 'bit':
				return new BitIntegerCapacity($capacity);
			
			case 'hexa':
				return new HexaIntegerCapacity($capacity);
			
			case 'char':
				return new StringIntegerCapacity($capacity);
				
			case 'byte':
			default:
				return new ByteIntegerCapacity($capacity);
		}
	}
	
}
