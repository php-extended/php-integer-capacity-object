<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Model\ByteIntegerCapacity;
use PHPUnit\Framework\TestCase;

/**
 * ByteIntegerCapacityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Model\ByteIntegerCapacity
 *
 * @internal
 *
 * @small
 */
class ByteIntegerCapacityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ByteIntegerCapacity
	 */
	protected ByteIntegerCapacity $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('12 bytes', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ByteIntegerCapacity(12);
	}
	
}
