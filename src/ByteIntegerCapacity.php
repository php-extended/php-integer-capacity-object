<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ByteIntegerCapacity class file.
 * 
 * This class represents an integer capacity in bytes.
 * 
 * @author Anastaszor
 */
class ByteIntegerCapacity implements IntegerCapacityInterface
{
	
	/**
	 * Builds a new ByteIntegerCapacity with the given other capacity.
	 * 
	 * @param IntegerCapacityInterface $capacity
	 * @return ByteIntegerCapacity
	 */
	public static function buildFromCapacity(IntegerCapacityInterface $capacity) : ByteIntegerCapacity
	{
		return new self($capacity->getNumberOfBytes());
	}
	
	/**
	 * The number of 8-bits bytes this capacity holds.
	 * 
	 * @var integer
	 */
	protected int $_bytes;
	
	/**
	 * Builds a new ByteIntegerCapacity with the given capacity in bytes.
	 * 
	 * @param integer $capacity the number of bytes
	 */
	public function __construct(int $capacity)
	{
		$this->_bytes = \max(1, $capacity);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ((string) $this->getNumberOfBytes()).' bytes';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBits()
	 */
	public function getNumberOfBits() : int
	{
		return $this->_bytes * 8;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBytes()
	 */
	public function getNumberOfBytes() : int
	{
		return $this->_bytes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase10()
	 */
	public function getNumberOfDigitsBase10() : int
	{
		return (int) \floor(\log10(8 ** $this->_bytes));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase16()
	 */
	public function getNumberOfDigitsBase16() : int
	{
		return (int) \floor(\log(8 ** $this->_bytes) / \log(16));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMinimumValue()
	 */
	public function getSignedMinimumValue() : string
	{
		return '-'.\bcpow('8', (string) ($this->_bytes - 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMaximumValue()
	 */
	public function getSignedMaximumValue() : string
	{
		return \bcsub(\bcpow('8', (string) ($this->_bytes - 1)), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getUnsignedMaximumValue()
	 */
	public function getUnsignedMaximumValue() : string
	{
		return \bcsub(\bcpow('8', (string) $this->_bytes), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::mergeWith()
	 * @psalm-suppress ArgumentTypeCoercion
	 */
	public function mergeWith(IntegerCapacityInterface $capacity) : IntegerCapacityInterface
	{
		/** @phpstan-ignore-next-line */
		if(0 < \bccomp($this->getUnsignedMaximumValue(), $capacity->getUnsignedMaximumValue()))
		{
			return $this;
		}
		
		return $capacity;
	}
	
}
