<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * HexaIntegerCapacity class file.
 * 
 * This class represents an integer capacity in hexadecimal digits.
 * 
 * @author Anastaszor
 */
class HexaIntegerCapacity implements IntegerCapacityInterface
{
	
	/**
	 * Builds a new HexaIntegerCapacity with the given other capacity.
	 * 
	 * @param IntegerCapacityInterface $capacity
	 * @return HexaIntegerCapacity
	 */
	public static function buildFromCapacity(IntegerCapacityInterface $capacity) : HexaIntegerCapacity
	{
		return new self($capacity->getNumberOfDigitsBase16());
	}
	
	/**
	 * The number of hexadecimal digits this capacity holds.
	 * 
	 * @var integer
	 */
	protected int $_hexaDigits;
	
	/**
	 * Builds a new HexaIntegerCapacity with the given capacity in hexadecimal
	 * digits.
	 * 
	 * @param integer $capacity the number of hexadecimal digits
	 */
	public function __construct(int $capacity)
	{
		$this->_hexaDigits = \max(1, $capacity);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ((string) $this->getNumberOfDigitsBase16()).' hexas';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBits()
	 */
	public function getNumberOfBits() : int
	{
		return $this->_hexaDigits * 16;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBytes()
	 */
	public function getNumberOfBytes() : int
	{
		return $this->_hexaDigits * 2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase10()
	 */
	public function getNumberOfDigitsBase10() : int
	{
		return (int) \floor(\log10(16 ** $this->_hexaDigits));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase16()
	 */
	public function getNumberOfDigitsBase16() : int
	{
		return $this->_hexaDigits;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMinimumValue()
	 */
	public function getSignedMinimumValue() : string
	{
		return '-'.\bcpow('16', (string) ($this->_hexaDigits - 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMaximumValue()
	 */
	public function getSignedMaximumValue() : string
	{
		return \bcsub(\bcpow('16', (string) ($this->_hexaDigits - 1)), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getUnsignedMaximumValue()
	 */
	public function getUnsignedMaximumValue() : string
	{
		return \bcsub(\bcpow('16', (string) $this->_hexaDigits), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::mergeWith()
	 * @psalm-suppress ArgumentTypeCoercion
	 */
	public function mergeWith(IntegerCapacityInterface $capacity) : IntegerCapacityInterface
	{
		/** @phpstan-ignore-next-line */
		if(0 < \bccomp($this->getUnsignedMaximumValue(), $capacity->getUnsignedMaximumValue()))
		{
			return $this;
		}
		
		return $capacity;
	}
	
}
