<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * StringIntegerCapacity class file.
 * 
 * This class represents an integer capacity in decimal digits.
 * 
 * @author Anastaszor
 */
class StringIntegerCapacity implements IntegerCapacityInterface
{
	
	/**
	 * Builds a new StringIntegerCapacity with the given other capacity.
	 * 
	 * @param IntegerCapacityInterface $capacity
	 * @return StringIntegerCapacity
	 */
	public static function buildFromCapacity(IntegerCapacityInterface $capacity) : StringIntegerCapacity
	{
		return new self($capacity->getNumberOfDigitsBase10());
	}
	
	/**
	 * The number of decimal digits this capacity holds.
	 * 
	 * @var integer
	 */
	protected int $_decDigits;
	
	/**
	 * Builds a new StringIntegerCapacity with the given capacity in decimal
	 * digits.
	 * 
	 * @param integer $capacity the number of decimal digits
	 */
	public function __construct(int $capacity)
	{
		$this->_decDigits = \max(1, $capacity);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ((string) $this->getNumberOfDigitsBase10()).' chars';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBits()
	 */
	public function getNumberOfBits() : int
	{
		return (int) \floor(\log(10 ** $this->_decDigits) / \log(2));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBytes()
	 */
	public function getNumberOfBytes() : int
	{
		return (int) \floor(\log(10 ** $this->_decDigits) / \log(8));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase10()
	 */
	public function getNumberOfDigitsBase10() : int
	{
		return $this->_decDigits;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase16()
	 */
	public function getNumberOfDigitsBase16() : int
	{
		return (int) \floor(\log(10 ** $this->_decDigits) / \log(16));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMinimumValue()
	 */
	public function getSignedMinimumValue() : string
	{
		return '-'.\bcsub(\bcpow('10', (string) ($this->_decDigits - 1)), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMaximumValue()
	 */
	public function getSignedMaximumValue() : string
	{
		return \bcsub(\bcpow('10', (string) ($this->_decDigits)), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getUnsignedMaximumValue()
	 */
	public function getUnsignedMaximumValue() : string
	{
		return \bcsub(\bcpow('10', (string) $this->_decDigits), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::mergeWith()
	 * @psalm-suppress ArgumentTypeCoercion
	 */
	public function mergeWith(IntegerCapacityInterface $capacity) : IntegerCapacityInterface
	{
		/** @phpstan-ignore-next-line */
		if(0 < \bccomp($this->getUnsignedMaximumValue(), $capacity->getUnsignedMaximumValue()))
		{
			return $this;
		}
		
		return $capacity;
	}
	
}
