<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-integer-capacity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * BitIntegerCapacity class file.
 * 
 * This class represents an integer capacity in bits. 
 * 
 * @author Anastaszor
 */
class BitIntegerCapacity implements IntegerCapacityInterface
{
	
	/**
	 * Builds a new BitIntegerCapacity with the given other capacity.
	 * 
	 * @param IntegerCapacityInterface $capacity
	 * @return BitIntegerCapacity
	 */
	public static function buildFromCapacity(IntegerCapacityInterface $capacity) : BitIntegerCapacity
	{
		return new self($capacity->getNumberOfBits());
	}
	
	/**
	 * The number of bits this capacity holds.
	 * 
	 * @var integer
	 */
	protected int $_bits;
	
	/**
	 * Builds a new BitIntegerCapacity with the given capacity in bits.
	 * 
	 * @param integer $capacity the number of bits
	 */
	public function __construct(int $capacity)
	{
		$this->_bits = \max(1, $capacity);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ((string) $this->getNumberOfBits()).' bits';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBits()
	 */
	public function getNumberOfBits() : int
	{
		return $this->_bits;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfBytes()
	 */
	public function getNumberOfBytes() : int
	{
		return (int) \floor(((float) $this->_bits) / 8.0);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase10()
	 */
	public function getNumberOfDigitsBase10() : int
	{
		return (int) \floor(\log10(2 ** $this->_bits));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getNumberOfDigitsBase16()
	 */
	public function getNumberOfDigitsBase16() : int
	{
		return (int) \floor(\log(2 ** $this->_bits) / \log(16));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMinimumValue()
	 */
	public function getSignedMinimumValue() : string
	{
		return '-'.\bcpow('2', (string) ($this->_bits - 1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getSignedMaximumValue()
	 */
	public function getSignedMaximumValue() : string
	{
		return \bcsub(\bcpow('2', (string) ($this->_bits - 1)), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::getUnsignedMaximumValue()
	 */
	public function getUnsignedMaximumValue() : string
	{
		return \bcsub(\bcpow('2', (string) $this->_bits), '1');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Model\IntegerCapacityInterface::mergeWith()
	 * @psalm-suppress ArgumentTypeCoercion
	 */
	public function mergeWith(IntegerCapacityInterface $capacity) : IntegerCapacityInterface
	{
		/** @phpstan-ignore-next-line */
		if(0 < \bccomp($this->getUnsignedMaximumValue(), $capacity->getUnsignedMaximumValue()))
		{
			return $this;
		}
		
		return $capacity;
	}
	
}
